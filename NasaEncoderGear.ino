#include <Arduino.h>

#define HOLD LOW
#define RELEASE HIGH
#define NASA_INTERRUPT 1        //interrupt number for pin 3

#define HALL_A_225_DEGREE_PEAK 800

//Encoder values for 22.50 degree intervals
#define ENCODER_BEGINNING 140
#define ENCODER_225 351
#define ENCODER_450 566
#define ENCODER_675 786

//Delay to take measurements
#define MOTOR_DELAY 2000

//Pin definitions
const int encoder_input = 14; //A0
const int hall_a = 15; //A1
const int hall_b = 16; //A2

const int nasa_input = 3; //D03
const int holding_torque = 4; //D04
const int motor_counter_clock_wise = 7; //D07
const int motor_clock_wise = 8; //D08

bool first_run = true;

void setup() {
    //Input Pins
    pinMode(encoder_input, INPUT); //encoder input
    pinMode(hall_a, INPUT); //Hall A input
    pinMode(hall_b, INPUT); //Hall B input
    pinMode(nasa_input, INPUT); //NASA input

    //Output Pins
    pinMode(motor_clock_wise, OUTPUT); //Motor CW as output
    pinMode(motor_counter_clock_wise, OUTPUT); //Motor CCW as output
    pinMode(holding_torque, OUTPUT); //Holding_Torque as output

    //Setting up NASA's input as an interrupt triggered when the pin is HOLD
    attachInterrupt(NASA_INTERRUPT, HoldUntilDirected, HOLD);
}

void turnTo(int destination_position) {

    int current_position = analogRead(encoder_input);

    if (current_position < destination_position) {

        //start up the motor counter clock wise
        digitalWrite(motor_clock_wise, HOLD);
        digitalWrite(motor_counter_clock_wise, RELEASE);

        //keep turning until the encoder is at the right position 
        while (analogRead(encoder_input) < destination_position);
        digitalWrite(motor_counter_clock_wise, HOLD);

        return;
    }


    if (current_position > destination_position) {

        //Start up the motor clock wise
        digitalWrite(motor_clock_wise, RELEASE);
        digitalWrite(motor_counter_clock_wise, HOLD);

        while (analogRead(encoder_input) > destination_position);
        digitalWrite(motor_clock_wise, HOLD);

        return;
    }

    if (current_position == destination_position)
        return;
}

void HoldUntilDirected() {
    //delays the program until the input tells us to move
    digitalWrite(motor_clock_wise, HOLD);
    digitalWrite(motor_counter_clock_wise, HOLD);
    while (digitalRead(nasa_input) == HOLD);
}

void loop() {
    //The way this needs to work is going counter clock wise to 67.50 and then
    //going clock wise back to 0 and repeating, this will work even though the
    //turnTo function doesn't take a direction parameter because turnTo
    //checks if it should rotate one way or the other, and due to the order
    //we call it, it should go first counter clock wise and then clock wise
    if (first_run) {
        //Gets the gear to 0
        turnTo(ENCODER_BEGINNING);
        delay(MOTOR_DELAY);
        first_run = false;
    }else{
        turnTo(ENCODER_BEGINNING);
        delay(2*MOTOR_DELAY);
    }
    
    //go to 22.50 degrees
    turnTo(ENCODER_225);
    delay(MOTOR_DELAY);

    //go to 45 degrees
    turnTo(ENCODER_450);
    delay(MOTOR_DELAY);

    //go to 67.50 degrees
    turnTo(ENCODER_675);
    delay(2 * MOTOR_DELAY);

    //go to 45 degrees
    turnTo(ENCODER_450);
    delay(MOTOR_DELAY);

    //go to 22.50 degrees
    turnTo(ENCODER_225);
    delay(MOTOR_DELAY);

    //this will then loop completing the cycle

}
